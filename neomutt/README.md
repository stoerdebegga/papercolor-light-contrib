# PaperColor Light Adaption for Neomutt

Adaption of [NLKNguyen](https://github.com/NLKNguyen)'s [PaperColor Light](https://github.com/NLKNguyen/papercolor-theme) for [neomutt](https://neomutt.org/).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/papercolor-light-contrib/raw/branch/main/neomutt/papercolor-light \
--create-dirs -o ${HOME}/.config/neomutt/papercolor-light
```

2. Edit ${HOME}/.config/neomutt/neomuttrc and add

```
source ~/.config/neomutt/papercolor-light
```

## Screenshots

![NEOMUTT001](.media/screenshot_001.png)
![NEOMUTT002](.media/screenshot_002.png)
![NEOMUTT003](.media/screenshot_003.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
