# PaperColor Light Adaption for Newsboat

Adaption of [NLKNguyen](https://github.com/NLKNguyen)'s [PaperColor Light](https://github.com/NLKNguyen/papercolor-theme) for [Newsboat](https://newsboat.org/).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/papercolor-light-contrib/raw/branch/main/newsboat/papercolor-light.colors \
--create-dirs -o ${HOME}/.config/newsboat/papercolor-light.colors
```

2. Edit ${HOME}/.config/newsboat/config and add

```
include "~/.config/newsboat/papercolor-light.colors"
```

## Screenshots

![NEWSBOAT01](.media/screenshot_001.png)
![NEWSBOAT02](.media/screenshot_002.png)
![NEWSBOAT03](.media/screenshot_003.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
