# PaperColor Light Adaption for Kitty

Adaption of [NLKNguyen](https://github.com/NLKNguyen)'s [PaperColor Light](https://github.com/NLKNguyen/papercolor-theme) for [Kitty](https://github.comkovidgoyal/kitty).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/papercolor-light-contrib/raw/branch/main/kitty/papercolor-light.conf \
--create-dirs -o ${HOME}/.config/kitty/papercolor-light.conf
```

2. Edit ${HOME}/.config/kitty/kitty.conf and add

```
include papercolor-light.conf
```

## Screenshots

![KITTY001](.media/screenshot.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
