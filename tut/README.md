# PaperColor Light Adaption for tut

Adaption of [NLKNguyen](https://github.com/NLKNguyen)'s [PaperColor Light](https://github.com/NLKNguyen/papercolor-theme) for [tut](https://github.com/RasmusLindroth/tut).

## Installation

1. Checkout the **tut** repository

```
git clone https://github.com/RasmusLindroth/tut.git
cd tut
```

2. Fetch the theme file

This should not be necessary in the majority of cases as the theme is part of the regular upstream release.

```
curl https://codeberg.org/stoerdebegga/papercolor-theme-contrib/raw/branch/main/tut/papercolor-light.ini \
-o themes/papercolor-light.ini
```

3. Install **tut* with new theme

```
go istall
```

## Screenshots

![tut01](.media/screenshot001.png)
![tut02](.media/screenshot002.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
