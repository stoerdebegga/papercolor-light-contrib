# PaperColor Light Adaption for Cmus

Adaption of [NLKNguyen](https://github.com/NLKNguyen)'s [PaperColor Light](https://github.com/NLKNguyen/papercolor-theme) for [cmus](https://cmus.github.io/).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/papercolor-light-contrib/raw/branch/main/cmus/papercolor-light.theme \
--create-dirs -o ${HOME}/.config/cmus/papercolor-light.theme
```

2. Edit ${HOME}/.config/cmus/rc and add or replace exisiting colorscheme

```
colorscheme papercolor-light
```

## Screenshots

![CMUS01](.media/screenshot001.png)
![CMUS02](.media/screenshot002.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
