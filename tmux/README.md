# PaperColor Light Adaption for tmux

Adaption of [NLKNguyen](https://github.com/NLKNguyen)'s [PaperColor Light](https://github.com/NLKNguyen/papercolor-theme) for [tmux](https://github.com/tmux/tmux/wiki).

## Installation

1. Install using tpm.

    ```tmux
    set -g @plugin 'https://codeberg.org/stoerdebegga/papercolor-light-contrib/tmux'
    ```

### Manual Installation

1. Clone this repository.

    ```sh
    git clone --depth=1 https://codeberg.org/stoerdebegga/papercolor-light-contrib/tmux.git
    ```

1. Add following to your `.tmux.conf`.

    ```tmux
    run-shell '. /path/to/papercolor-light-tmux/papercolor-light.tmux'
    ```

## Screenshots

![TMUX001](.media/screenshot.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
