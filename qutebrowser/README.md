# PaperColor Light Adaption for Qutebrowser

Adaption of [NLKNguyen](https://github.com/NLKNguyen)'s [PaperColor Light](https://github.com/NLKNguyen/papercolor-theme) for [qutebrowser](https://qutebrowser.org/).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/papercolor-light-contrib/raw/branch/main/qutebrowser/papercolor-light.py \
--create-dirs -o ${HOME}/.config/qutebrowser/papercolor-light.py
```

2. Edit ${HOME}/.config/qutebrowser/config.py and add

```
config.source('papercolor-light.py')
```

at the end of your config file.

## Screenshots

![QUTEBROWSER](.media/screenshot.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
