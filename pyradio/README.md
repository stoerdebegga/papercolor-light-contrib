# PaperColor Light Adaption for PyRadio

Adaption of [NLKNguyen](https://github.com/NLKNguyen)'s [PaperColor Light](https://github.com/NLKNguyen/papercolor-theme) for [pyradio](https://www.coderholic.com/pyradio/).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/papercolor-light-contrib/raw/branch/main/pyradio/papercolor-light.pyradio-theme \
--create-dirs -o ${HOME}/.config/pyradio/themes/papercolor-light.pyradio-theme
```

In case you are using PyRadio version *0.8.9.21*

```sh
curl https://codeberg.org/stoerdebegga/papercolor-light-contrib/raw/branch/main/pyradio/papercolor-light-ng.pyradio-theme \
--create-dirs -o ${HOME}/.config/pyradio/themes/papercolor-light-ng.pyradio-theme
```

2. Edit ${HOME}/.config/pyradio/config and add or replace existing theme

```
theme = papercolor-light
```

## Screenshots

![PYRADIO01](.media/screenshot.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
